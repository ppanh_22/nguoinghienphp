<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;
use DB;
use Session;
use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;
session_start();

class AdminOrderController extends Controller
{
    public function AuthLogin(){
        $id_admin =Session::get('id_admin');
        if($id_admin){
            return Redirect::to('dashboard');
        }else{
            return Redirect::to('admin')->send();
        }
        // $this->AuthLogin();
    }

    public function all_order() {
        $this->AuthLogin();
        $order = DB::table('orders')
        ->join('accounts', 'accounts.id_account', '=', 'orders.id_account')->get();
        return view('admin.order.all-order')->with('order', $order);
    }
    public function all_orderdetail($id_order) {
        $this->AuthLogin();
        $orderdetails = DB::table('orderdetails')->where('id_order',$id_order)->join('products', 'products.id_product', '=', 'orderdetails.id_product')->get();
        return view('admin.order.order-detail')->with('orderdetails', $orderdetails);
    }
}
