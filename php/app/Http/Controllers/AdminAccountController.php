<?php

namespace App\Http\Controllers;

use DB;
use Session;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests;

session_start();

class AdminAccountController extends Controller
{
    public function AuthLogin(){
        $id_admin =Session::get('id_admin');
        if($id_admin){
            return Redirect::to('dashboard');
        }else{
            return Redirect::to('admin')->send();
        }
        // $this->AuthLogin();
    }

    public function all_account() {
        $this->AuthLogin();
        $account = DB::table('accounts')->get();
        return view('admin.account.all-account')->with('account', $account);
    }
}
