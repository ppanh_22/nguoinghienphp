<?php

namespace App\Http\Controllers;

use DB;
use Session;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests;
use SebastianBergmann\Environment\Console;

session_set_cookie_params(0);
session_start();

class CartController extends Controller
{

    public function index(){
        $category = DB::table('categories')->orderby('id_category','desc')->get();

        return view('pages.cart')->with('category', $category);
    }

    public function addToCart(Request $request){
        $product_id = $request->product_id;
        $name = $request->product_name;
        $price = $request->product_price;
        $quantity = $request->product_quanity;
        $image = $request->product_image;
        $size = $request->product_size;

        $cartItemId = $product_id.'_'.$size;

        $cart = array();
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        if($oldCart){
            $cart = $oldCart;
        }

        if(array_key_exists($cartItemId, $cart)){
            $cart[$cartItemId]['quantity'] += $quantity;
        }else{
            $cart[$cartItemId]['cartItemId'] = $cartItemId;
            $cart[$cartItemId]['id'] = $product_id;
            $cart[$cartItemId]['name'] = $name;
            $cart[$cartItemId]['price'] = $price;
            $cart[$cartItemId]['quantity'] = $quantity;
            $cart[$cartItemId]['image'] = $image;
            $cart[$cartItemId]['size'] = $size;
        }
        Session::put('cart', $cart);
        return Redirect::to('/cart');
    }

    public function deleteCart($cartItemId){
        $cart = Session::has('cart') ? Session::get('cart') : null;
        if(array_key_exists($cartItemId, $cart)){
            unset($cart[$cartItemId]);
        }
        Session::put('cart', $cart);
        return Redirect::to('/cart');
    }
}
