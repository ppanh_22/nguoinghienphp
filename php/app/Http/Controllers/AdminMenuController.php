<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;
use DB;
use Session;
use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Requeste;
session_start();

class AdminMenuController extends Controller
{
    public function AuthLogin(){
        $id_admin =Session::get('id_admin');
        if($id_admin){
            return Redirect::to('dashboard');
        }else{
            return Redirect::to('admin')->send();
        }
        // $this->AuthLogin();
    }

    public  function  save_menu(Request $request){
        $this->AuthLogin();
        $data =array();
        $data['menu_name'] = $request->menu_name;
        $data['url'] = $request->menu_url;
        DB::table('menus')->insert($data);
        Session::put('massage', 'Thêm menu thành công');
        return Redirect::to('/admin/all-menu');
    }

    public function all_menu() {
        $this->AuthLogin();
        $menu = DB::table('menus')->get();
        return view('admin.menu.all-menu')->with('menu', $menu);
    }

    public function  add_menu(){
        $this->AuthLogin();
        return view('admin.menu.add-menu');
    }

    public function delete_menu($id_menu){
        $this->AuthLogin();
        DB::table('menus')->where('id_menu',$id_menu)->delete();
        return Redirect::to('/admin/all-menu');
    }

    public function edit_menu($id_menu){
        $this->AuthLogin();
        $edit_menu = DB::table('menus')->where('id_menu',$id_menu)->get();
        $manager_menu = view('admin.menu.edit-menu')->with('edit_menu', $edit_menu);
        return view('layoutAdmin')->with('admin.menu.edit-menu', $manager_menu);
    }

    public function update_menu(Request $request, $id_menu){
        $this->AuthLogin();
        $data = array();
        $data['menu_name'] = $request->menu_name;
        $data['url']= $request->menu_url;
        DB::table('menus')->where('id_menu',$id_menu)->update($data);


        return Redirect::to('/admin/all-menu');
    }
}
