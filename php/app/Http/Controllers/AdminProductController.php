<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;
use DB;
use Session;
use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;
session_start();

class AdminProductController extends Controller
{
    public function AuthLogin(){
        $id_admin =Session::get('id_admin');
        if($id_admin){
            return Redirect::to('dashboard');
        }else{
            return Redirect::to('admin')->send();
        }
        // $this->AuthLogin();
    }

    public function all_product() {
        $this->AuthLogin();
        $product = DB::table('products')
            ->join('categories', 'categories.id_category', '=', 'products.id_category')->get();
        return view('admin.product.all-product')->with('product', $product);
    }

    public function add_product(){
        $this->AuthLogin();
        $pro =DB::table('products')->orderby('id_product','desc')->get();
        $category = DB::table('categories')->orderby('id_category','desc')->get();
        return view('admin.product.add-product')->with('pro', $pro)->with('category', $category);
//        return view('admin.product.add-product');
    }

    public  function save_product(Request $request){
        $this->AuthLogin();
        $data =array();
        $data['product_name'] = $request->product_name;
        $data['id_category'] = $request->product_id_category;
        $data['colors']= $request->product_colors;
        $data['sale']= $request->product_sale;
        $data['title']= $request->product_title;
        $data['highlight']= $request->product_highlight;
        $data['new_product']= $request->product_new_product;
        $data['detail']= $request->product_detail;
        $data['price']= $request->product_price;
        $data['image']= $request->product_image;
        $data['image1']= $request->product_image1;
        $data['image2']= $request->product_image2;
        $data['image3']= $request->product_image3;
        $data['count']= $request->product_count;
        $data['created_at']= Carbon::now();
//        $data['updated_at']= Carbon::now();
        DB::table('products')->insert($data);
        Session::put('massage', 'Thêm sản phẩm thành công');
        return Redirect::to('/admin/all-product');

    }

    public function delete_product($id_product){
        $this->AuthLogin();
        DB::table('products')->where('id_product',$id_product)->delete();
        return Redirect::to('/admin/all-product');
    }

    public function edit_product($id_product){
        $this->AuthLogin();
        $edit_product = DB::table('products')->where('id_product',$id_product)->get();
        $category = DB::table('categories')->orderby('id_category', 'desc')->get();
        $manager_product = view('admin.product.edit-product')->with('edit_product', $edit_product)->with('category', $category);
        return view('layoutAdmin')->with('admin.product.edit-product', $manager_product);
    }

    public function update_product(Request $request,$id_product){
        $this->AuthLogin();
        $data = array();
        $data['product_name'] = $request->product_name;
        $data['id_category'] = $request->product_id_category;
        $data['colors']= $request->product_colors;
        $data['sale']= $request->product_sale;
        $data['title']= $request->product_title;
        $data['highlight']= $request->product_highlight;
        $data['new_product']= $request->product_new_product;
        $data['detail']= $request->product_detail;
        $data['price']= $request->product_price;
//        $get_image = $request->file('product_image');
        $data['image']= $request->product_image;
//        if($get_image){
//            $get_name_image = $get_image->getClientOriginalName();
//            $name_image = current(explode('.',$get_name_image));
//            $new_image = $name_image.rand(0,99).'.'.$get_image->getClientOriginalExtension();
//            $get_image->move('/public/backend/images/',$new_image);
//            $data['image'] = $new_image;
//            DB::table('product')->where('id_product',$id_product)->update($data);
//            Session::put('message', 'Cập nhật sản phẩm thành công');
//            return Redirect::to('add-product');
//        }else{
//            $data['image'] = $request->product_image;
//        }
        $data['image1']= $request->product_image1;
        $data['image2']= $request->product_image2;
        $data['image3']= $request->product_image3;
        $data['count']= $request->product_count;
        $data['updated_at']= Carbon::now();
        DB::table('products')->where('id_product',$id_product)->update($data);
        Session::put('massage', 'Cập nhật phẩm thành công');

        return Redirect::to('admin/all-product');
    }
}
