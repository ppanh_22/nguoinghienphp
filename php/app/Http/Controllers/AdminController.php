<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use DB;
use Session;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests;

session_start();

class AdminController extends Controller
{
    public function AuthLogin(){
        $id_admin = Session::get('id_admin');
        if($id_admin){
            return Redirect::to('admin/dashboard');
        }else{
            return Redirect::to('admin')->send();
        }
    }

    public function index() {
        return view('admin_login');
    }

    public function show_dashboard(){
        $this->AuthLogin();
        return view('admin.dashboard.dashboard');

    }

    public function dashboard(Request $request){
        $admin_email = $request->admin_email;
        $admin_password = md5($request->admin_password);
        $result = DB::table('admins')->where('admin_email',$admin_email)->where('admin_password',$admin_password)->first();
        if($result){
            Session::put('admin_name',$result->admin_name);
            Session::put('id_admin',$result->id_admin);
            return Redirect::to('/admin/dashboard');
        }else{
            Session::put('message', 'Mật khẩu hoặc tài khoản đã nhập sai. Yêu cầu nhập lại');
            return Redirect::to('/admin');
        }
    }
    public function logout(){
        $this->AuthLogin();
        Session::put('admin_name',null);
        Session::put('id_admin',null);
        return Redirect::to('/admin');
    }

}
