<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;
use DB;
use Session;
use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;
session_start();

class AdminSlideController extends Controller
{
    public function AuthLogin(){
        $id_admin =Session::get('id_admin');
        if($id_admin){
            return Redirect::to('dashboard');
        }else{
            return Redirect::to('admin')->send();
        }
        // $this->AuthLogin();
    }

    public  function  save_slide(Request $request){
        $this->AuthLogin();
        $data =array();
        $data['img'] = $request->slide_img;
        $data['caption'] = $request->slide_caption;
        $data['content'] = $request->slide_content;
        DB::table('slides')->insert( $data );
        return Redirect::to('/admin/all-slide');

    }

    public function all_slide() {
        $this->AuthLogin();
        $slide = DB::table('slides')->get();
        return view('admin.slide.all-slide')->with('slide', $slide);
    }

    public function  add_slide(){
        $this->AuthLogin();
        return view('admin.slide.add-slide');
    }

    public function delete_slide($id_slide){
        $this->AuthLogin();
        DB::table('slides')->where('id_slide',$id_slide)->delete();
        return Redirect::to('/admin/all-slide');
    }

    public function edit_slide($id_slide){
        $this->AuthLogin();
        $edit_slide = DB::table('slides')->where('id_slide',$id_slide)->get();
        $manager_slide = view('admin.slide.edit-slide')->with('edit_slide', $edit_slide);
        return view('layoutAdmin')->with('admin.slide.edit-slide', $manager_slide);
    }

    public function update_slide(Request $request,$id_slide){
        $this->AuthLogin();
        $data = array();
        $data['img'] = $request->slide_img;
        $data['caption']= $request->slide_caption;
        $data['content']= $request->slide_content;
        DB::table('slides')->where('id_slide',$id_slide)->update($data);


        return Redirect::to('/admin/all-slide');
    }
}
