<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;
use DB;
use Session;
use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;
session_start();

class AdminSizeController extends Controller
{
    public function AuthLogin(){
        $id_admin =Session::get('id_admin');
        if($id_admin){
            return Redirect::to('dashboard');
        }else{
            return Redirect::to('admin')->send();
        }
        // $this->AuthLogin();
    }

    public  function  save_size(Request $request){
        $this->AuthLogin();
        $data =array();
        $data['id_product'] = $request->size_id_product;
        $data['name_size'] = $request->size_name;
        $data['quantity'] = $request->size_quantity;
        $data['created_at']= Carbon::now();
        $data['updated_at']= Carbon::now();
        DB::table('sizes')->insert( $data );
        return Redirect::to('/admin/all-size');

    }

    public function all_size() {
        $this->AuthLogin();
        $size = DB::table('sizes')
            ->join('products', 'products.id_product', '=', 'sizes.id_product')->get();
        return view('admin.size.all-size')->with('size', $size);
    }

    public function  add_size(){
        $this->AuthLogin();
        $size = DB::table('sizes')->orderby('id_size','desc')->get();
        $pro =DB::table('products')->orderby('id_product','desc')->get();
        return view('admin.size.add-size')->with('size', $size)->with('pro', $pro);
    }

    public function delete_size($id_size)
    {
        $this->AuthLogin();
        DB::table('sizes')->where('id_size', $id_size)->delete();
        return Redirect::to('/admin/all-size');
    }

    public function edit_size($id_size){
        $this->AuthLogin();
        $edit_size = DB::table('sizes')->where('id_size',$id_size)->get();
        $pro =DB::table('products')->orderby('id_product','desc')->get();
        $manager_size = view('admin.size.edit-size')->with('edit_size', $edit_size)->with('pro', $pro);
        return view('layoutAdmin')->with('admin.size.edit-size', $manager_size);
    }

    public function update_size(Request $request, $id_size){
        $this->AuthLogin();
        $data = array();
        $data['id_product'] = $request->size_id_product;
        $data['name_size'] = $request->size_name;
        $data['quantity'] = $request->size_quantity;
        $data['created_at']= Carbon::now();
        $data['updated_at']= Carbon::now();
        DB::table('sizes')->where('id_size',$id_size)->update($data);

        return Redirect::to('/admin/all-size');
    }
}
