<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use DB;
use Session;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests;

session_start();

class AdminCategoryController extends Controller
{
    public function AuthLogin(){
        $id_admin =Session::get('id_admin');
        if($id_admin){
            return Redirect::to('dashboard');
        }else{
            return Redirect::to('admin')->send();
        }
        // $this->AuthLogin();
    }

    public function  add_category(){
        $this->AuthLogin();
        return view('admin.category.add-category');
    }

    public  function  save_category(Request $request){
        $this->AuthLogin();
        $data =array();
        $data['category_name'] = $request->category_name;
        $data['mota']= $request->category_mota;
        $data['img']= $request->category_img;
        $data['created_at']= Carbon::now();
        $data['updated_at']= Carbon::now();
        DB::table('categories')->insert($data);
        Session::put('message', 'Thêm danh mục sản phẩm thành công');
        return Redirect::to('/admin/all-category');

    }

    public function all_category() {
        $this->AuthLogin();
        $category = DB::table('categories')->get();
        return view('admin.category.all-category')->with('category', $category);
    }

    public function delete_category($id_category){
        $this->AuthLogin();
        DB::table('categories')->where('id_category',$id_category)->delete();
    return Redirect::to('/admin/all-category');
    }

    public function edit_category($id_category){
        $this->AuthLogin();
        $edit_category = DB::table('categories')->where('id_category',$id_category)->get();
        $manager_category = view('admin.category.edit-category')->with('edit_category', $edit_category);
        return view('layoutAdmin')->with('admin.category.edit-category', $manager_category);
    }

    public function update_category(Request $request,$id_category){
        $this->AuthLogin();
        $data = array();
        $data['category_name'] = $request->category_name;
        $data['mota']= $request->category_mota;
//        $get_img = file('category_img');
//        if($get_img) {
//
//        }
        $data['img']= $request->category_img;
        $data['created_at']= Carbon::now();
        $data['updated_at']= Carbon::now();
        DB::table('categories')->where('id_category',$id_category)->update($data);


        return Redirect::to('admin/all-category');
    }

    public function show_category_product($id_category){
        $this->AuthLogin();
        $category = DB::table('categoríes')->orderby('id_category', 'desc')->paginate(9);
        $cate_product = DB::table('products')
            ->join('categories', 'categories.id_category', '=', 'products.id_category')
                ->where('products.id_category', $id_category)->paginate(9);
        return view('pages.show_category_product')->with('category', $category)->with('cate_product', $cate_product);
    }

}
