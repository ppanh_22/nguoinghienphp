<?php

namespace App\Http\Controllers;

use DB;
use Session;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests;
use SebastianBergmann\Environment\Console;

session_set_cookie_params(0);
session_start();

class OrderController extends Controller
{

    public function index()
    {
        $id_account = Session::get('id_account');
        if (!$id_account) {
            return Redirect::action('HomeController@index');
        }
        $orders = DB::table('orders')->where('id_account', $id_account)->orderby('created_at', 'desc')->get();
        foreach ($orders as $order) {
            $order->order_detail = DB::table('orderdetails')->where('id_order', $order->id_order)->get();
            foreach ($order->order_detail as $detail) {
                $detail->product = DB::table('products')->where('id_product', $detail->id_product)->first();
            }
        }
        $category = DB::table('categories')->orderby('id_category', 'desc')->get();
        $userName = Session::get('account_name');
        return view('pages.order')->with('orders', $orders)->with('category', $category)->with('userName', $userName);
    }

    public function placeOrder(Request $request)
    {
        $id_account = Session::get('id_account');
        $ship_name = $request->shipname;
        $ship_address = $request->shipaddress;
        $ship_mobile = $request->shipphone;
        $ship_email = $request->shipemail;
        $note = $request->shipnote;
        $ship_payment_method = "Tiền mặt";
        $total_sum = $request->total;
        $create_at = Carbon::now();
        $update_at = Carbon::now();
        $data = array();
        $data['id_account'] = $id_account;
        $data['ship_name'] = $ship_name;
        $data['ship_address'] = $ship_address;
        $data['ship_mobile'] = $ship_mobile;
        $data['ship_email'] = $ship_email;
        $data['note'] = $note;
        $data['ship_payment_method'] = $ship_payment_method;
        $data['total_sum'] = $total_sum;
        $data['created_at'] = $create_at;
        $data['updated_at'] = $update_at;
        $order = DB::table('orders')->insert($data);
        $id_order = DB::getPdo()->lastInsertId();
        $cart = Session::get('cart');
        foreach ($cart as $key => $value) {
            $data = array();
            $data['id_order'] = $id_order;
            $data['id_product'] = $value['id'];
            $data['quantity'] = $value['quantity'];
            $data['price'] = $value['price'];
            $data['size'] = $value['size'];
            $data['amount'] = $value['quantity'] * $value['price'];
            $data['created_at'] = $create_at;
            $data['updated_at'] = $update_at;
            $order_detail = DB::table('orderdetails')->insert($data);
        }
        //reduce product quantity
        foreach ($cart as $key => $value) {
            $data = array();
            $data['name_size'] = $value['size'];
            $product = DB::table('sizes')->where('id_product', $value['id'])->where('name_size', $value['size'])->first();
            $quantity = $product->quantity - $value['quantity'];
            $data['quantity'] = $quantity;
            $product = DB::table('sizes')->where('id_product', $value['id'])->where('name_size', $value['size'])->update($data);
        }
        Session::put('cart', null);
        return Redirect::action('OrderController@index');
    }
}
