<?php

namespace App\Http\Controllers;

use DB;
use Session;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests;
use Illuminate\Contracts\Session\Session as SessionSession;
use Illuminate\Support\Facades\Session as FacadesSession;
use SebastianBergmann\Environment\Console;


session_start();

class HomeController extends Controller
{
    public function index() {
        $pro =DB::table('products')->where('highlight', '1')->limit(3)->orderby('id_product','desc')->get();
        $category = DB::table('categories')->orderby('id_category','desc')->get();
        $new_pro =DB::table('products')->where('new_product', '1')->limit(3)->orderby('id_product','desc')->get();
        $slides =DB::table('slides')->get();
        return view('pages.home')
        ->with('pro', $pro)->with('newpro', $new_pro)
        ->with('category', $category)
        ->with('slides', $slides);
    }

    public function contact(){
        $category = DB::table('categories')->orderby('id_category','desc')->get();
        return view('pages.contact')->with('category', $category);
    }

    public function contact_save(Request $request){
        $data = array();
        $data['name'] = $request->name;
        $data['email'] = $request->email;
        $data['title'] = $request->title;
        $data['description'] = $request->description;
        DB::table('contact')->insert($data);
        return Redirect::to('/contact')->with('message', 'State saved correctly!!!');
    }

    public function about()
    {
        $category = DB::table('categories')->orderby('id_category', 'desc')->get();
        return view('pages.about')->with('category', $category);
    }

    public function login()
    {
        return view('login');
    }
    public function shop() {
        $category = DB::table('categories')->orderby('id_category','desc')->get();
        $product = DB::table('products')->orderby('id_product','desc')->paginate(9);
        return view('pages.shop')->with('category', $category)->with('product', $product);
    }

    public function logout()
    {
        Session::put('account_name',null);
        Session::put('id_account',null);
        Session::put('account_email',null);
        Session::put('account_phone',null);
        Session::put('account_address',null);
        Session::put('cart',null);
        return Redirect::action('HomeController@index');
    }

    public function dangxuat()
    {
        return Redirect::action('HomeController@index');
    }

    public function catelayout() {
        $category = DB::table('categories')->orderby('id_category','desc')->get();
        return view('layout')->with('category', $category);
    }


    //tim kiem
    public function getProductSearch(Request $request)
    {
        $product = DB::table('products')->where('product_name', 'LIKE', '%' . $request->name_product . '%')->paginate(9);
        $category = DB::table('categories')->orderby('id_category','desc')->get();
        return view('pages.shop')->with('category', $category)->with('product', $product);
    }

    //Lấy sản phẩm theo danh mục
    public function product_cate($id_category) {
        $product = DB::table('products')
        ->where('products.id_category', $id_category)
        ->paginate(10);

        $category = DB::table('categories')->orderby('id_category','desc')->get();
        return view('pages.shop')->with('category', $category)->with('product', $product);
    }

    //Chi tiet san pham
    public function detailproduct($id_product) {
        $detail_product = DB::table('products')
        ->join('categories', 'categories.id_category', '=', 'products.id_category')
        ->where('products.id_product', $id_product)
        ->first();

        $size_product = DB::table('sizes')
        ->where('id_product', $id_product)->orderby('name_size')
        ->get();

        $product_lienquan = DB::table('products')
        ->where('id_category', '=' ,$detail_product->id_category)
        ->where('id_product','<>',$detail_product->id_product)
        ->get();

        $category = DB::table('categories')->orderby('id_category','desc')->get();

        return view('pages.detailproduct')
            ->with('detail', $detail_product)
            ->with('category', $category)
            ->with('size_product', $size_product)
            ->with('product_lienquan', $product_lienquan);

    }

    public function loginUser(Request $request)
    {
        $account_name = $request->account_name;
        $password = md5($request->password);

        $result = DB::table('accounts')->where('account_name', $account_name)->where('password', $password)->first();

        if($result){
            Session::put('account_name',$result->account_name);
            Session::put('account_address',$result->address);
            Session::put('account_phone',$result->phone);
            Session::put('account_email',$result->email);
            Session::put('id_account',$result->id_account);
            return Redirect::action('HomeController@index');
        }else{
            Session::put('message','Mật khẩu hoặc tài khoản sai. Vui lòng nhập lại!');
            return view('login');
        }
        // echo '<pre>';
        // print_r($result);
        // echo '</pre>';

    }


    //Dang ky tai khoan
    public function register() {
        return view('register');
    }

    public function create_account(Request $request){
        $check_username = DB::table('accounts')
        ->where('account_name', '=' ,$request->account_name)
        ->first();

        $check_phone = DB::table('accounts')
        ->where('phone', '=' ,$request->phone)
        ->first();

        $check_email= DB::table('accounts')
        ->where('email', '=' ,$request->email)
        ->first();

        if($check_username != NULL)
        {
            $warning = "Tài khoản đã được đăng ký, vui lòng nhập tên tài khoản khác !";
            return Redirect::action('HomeController@register')->withErrors($warning);
        }

        if($check_phone != NULL)
        {
            $warning = "Số điện thoại đã được đăng ký, vui lòng nhập số điện thoại khác !";
            return Redirect::action('HomeController@register')->withErrors($warning);
        }

        if($check_email != NULL)
        {
            $warning = "Email đã được đăng ký, vui lòng nhập email khác !";
            return Redirect::action('HomeController@register')->withErrors($warning);
        }

        $data = array();
        $data['account_name'] = $request->account_name;
        $data['phone'] = $request->phone;
        $data['email'] = $request->email;
        $data['password'] = md5($request->password);
        $data['address'] = $request->address;
        $data['created_at']= Carbon::now();
        $data['updated_at']= Carbon::now();
        DB::table('accounts')->insert($data);
        return Redirect::to('/login');

    }

    public function cart()
    {
        return view('pages.cart');
    }



    public function search(Request $request){

        $keyword = $request->keywords_submit;

        $search_product = DB::table('products')->where('product_name', 'like', '%' .$keyword. '%')->get();

        return view('pages.search')->with('search_product', $search_product);
    }

    public function searchFullText(Request $request)
    {
        dd($request->search);
    }


}
