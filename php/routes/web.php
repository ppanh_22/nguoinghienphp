<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//User
Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index');
Route::get('/contact', 'HomeController@contact');
Route::post('/contact-save', 'HomeController@contact_save');
Route::get('/about', 'HomeController@about');
Route::get('/login', 'HomeController@login')->name("user.login");
Route::get('/logout-user', 'HomeController@logout')->name("user.logout_user");
Route::get('/shop', 'HomeController@shop');
Route::get('/detailproduct/{id}', 'HomeController@detailproduct');
Route::get('/product-cate/{id}', 'HomeController@product_cate');
Route::get('/register', 'HomeController@register')->name("user.register");
Route::post('/create-account', 'HomeController@create_account');

Route::post('/search-product', 'HomeController@getProductSearch');


//Back-end
Route::post('/login-user', 'HomeController@loginUser');
Route::get('/category_product/{id}', 'AdminCategoryController@show_category_product');


//Admin
Route::get('/admin', 'AdminController@index');
Route::get('/admin/dashboard', 'AdminController@show_dashboard');

Route::get('/logout', 'AdminController@logout');
Route::post('/admin-dashboard', 'AdminController@dashboard');


//category
Route::get('/admin/all-category', 'AdminCategoryController@all_category');
Route::get('/admin/add-category', 'AdminCategoryController@add_category');
Route::post('/admin/save-category', 'AdminCategoryController@save_category');
Route::get('/admin/delete-category/{id}', 'AdminCategoryController@delete_category');
Route::get('/admin/edit-category/{id}','AdminCategoryController@edit_category');
Route::post('/admin/update-category/{id}','AdminCategoryController@update_category');


//menu
Route::get('/admin/all-menu', 'AdminMenuController@all_menu');
Route::get('/admin/add-menu', 'AdminMenuController@add_menu');
Route::post('/admin/save-menu', 'AdminMenuController@save_menu');
Route::get('/admin/delete-menu/{id}', 'AdminMenuController@delete_menu');
Route::get('/admin/edit-menu/{id}','AdminMenuController@edit_menu');
Route::post('/admin/update-menu/{id}','AdminMenuController@update_menu');



//slide
Route::get('/admin/all-slide', 'AdminSlideController@all_slide');
Route::get('/admin/add-slide', 'AdminSlideController@add_slide');
Route::post('/admin/save-slide', 'AdminSlideController@save_slide');
Route::get('/admin/delete-slide/{id}', 'AdminSlideController@delete_slide');
Route::get('/admin/edit-slide/{id}','AdminSlideController@edit_slide');
Route::post('/admin/update-slide/{id}','AdminSlideController@update_slide');


//Size
Route::get('/admin/all-size', 'AdminSizeController@all_size');
Route::get('/admin/add-size', 'AdminSizeController@add_size');
Route::post('/admin/save-size', 'AdminSizeController@save_size');
Route::get('/admin/delete-size/{id}', 'AdminSizeController@delete_size');
Route::get('/admin/edit-size/{id}','AdminSizeController@edit_size');
Route::post('/admin/update-size/{id}','AdminSizeController@update_size');


//Order
Route::get('/admin/all-order', 'AdminOrderController@all_order');
Route::get('/admin/order-detail/{id_order}', 'AdminOrderController@all_orderdetail');

//search
Route::post('/search', 'HomeController@search');


//product
Route::get('/admin/all-product', 'AdminProductController@all_product');
Route::get('/admin/add-product', 'AdminProductController@add_product');
Route::post('/admin/save-product', 'AdminProductController@save_product');
Route::get('/admin/delete-product/{id}', 'AdminProductController@delete_product');
Route::get('/admin/edit-product/{id}','AdminProductController@edit_product');
Route::post('/admin/update-product/{id}','AdminProductController@update_product');


//account
Route::get('/admin/all-account', 'AdminAccountController@all_account');

//cart
Route::get('/cart', 'CartController@index');
Route::post('/add-to-cart', 'CartController@addToCart');
Route::get('/delete-cart/{id}', 'CartController@deleteCart');
Route::post('/place-order', 'OrderController@placeOrder');
Route::get('/order', 'OrderController@index')->name('order');
