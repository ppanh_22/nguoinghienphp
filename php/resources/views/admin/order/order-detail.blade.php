@extends('layoutAdmin')
@section('title', 'Quản lý đơn hàng')
@section('AdminContent')
    <h2>Chi tiết đơn hàng</h2>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped" method="Get">
                                <thead>
                                <tr>
                                    <th>Tên sản phẩm</th>
                                    <th>Đơn giá</th>
                                    <th>Size</th>
                                    <th>Số lượng</th>
                                    <th>Thành tiền</th>
                                    <th>Ngày đặt</th>

                                </tr>
                                </thead>

                                <tbody>
                                @foreach($orderdetails as $key => $order)
                                    <tr>
                                        <td>
                                            {{$order->product_name}}
                                        </td>
                                        <td>
                                            {{number_format($order->price)}}
                                        </td>
                                        <td>
                                            {{number_format($order->size)}}
                                        </td>
                                        <td>
                                            {{$order->quantity}}
                                        </td>
                                        <td>
                                            {{number_format($order->amount)}}
                                        </td>
                                        <td>
                                            {{$order->created_at}}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    {{--    {{URL::to('/all-billdetail/'.$order->bill_id)}}--}}


@endsection
