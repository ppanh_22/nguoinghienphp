@extends('layoutAdmin')
@section('title', 'Quản lý đơn hàng')
@section('AdminContent')
    <h2>Quản lý đơn hàng</h2>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped" method="Get">
                                <thead>
                                <tr>
                                    <th>Mã hóa đơn</th>
                                    <th>Tên khách hàng</th>
                                    <th>Tên người nhận</th>
                                    <th>Số điện thoại</th>
                                    <th>Địa chỉ</th>
                                    <th>Email</th>
                                    <th>Phương thức thanh toán</th>
                                    <th>Ghi chú</th>
                                    <th>Tổng Tiền </th>
                                    <th>Ngày đặt</th>
                                    <th></th>

                                </tr>
                                </thead>

                                <tbody>
                                @foreach($order as $key => $bill)
                                    <tr>
                                        <td>
                                            {{$bill->id_order}}
                                        </td>
                                        <td>
                                            {{$bill->account_name}}
                                        </td>

                                        <td>
                                            {{$bill->ship_name}}
                                        </td>
                                        <td>
                                            {{$bill->ship_mobile}}
                                        </td>
                                        <td>
                                            {{$bill->ship_address}}
                                        </td>
                                        <td>
                                            {{$bill->ship_email}}
                                        </td>
                                        <td>
                                            {{$bill->ship_payment_method}}
                                        </td>
                                        <td>
                                            {{$bill->note}}
                                        </td>
                                        <td>
                                            {{number_format($bill->total_sum)}}
                                        </td>
                                        <td>
                                            {{$bill->created_at}}
                                        </td>
                                        <td>

                                            <a class='btn btn-default btn-sm' style='margin-left:5px' href="{{URL::to('/admin/order-detail/'.$bill->id_order)}}"><i class='fa fa-details'></i>Chi tiết</a>

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
{{--    {{URL::to('/all-billdetail/'.$bill->bill_id)}}--}}


@endsection
