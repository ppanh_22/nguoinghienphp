@extends('layoutAdmin')
@section('title', 'Quản lý sản phẩm')
@section('AdminContent')
    <h2>Quản lý sản phẩm</h2>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <!-- /.card-header -->

                        <div class="card-body">
                            <a class="btn btn-success" onclick="confirmAction()" style="margin-bottom:10px"
                               href="{{URL::to('/admin/add-product')}}"><i class="fa fa-plus"></i></a>
                            <table id="example1" class="table table-bordered table-striped" method="Get">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Danh mục</th>
                                    <th>Tên sản phẩm</th>
                                    <th>Màu</th>
                                    <th>Sale</th>
                                    <th>Title</th>
                                    <th>Nổi bật</th>
                                    <th>Sản phẩm mới</th>
                                    <th>Chi tiết</th>
                                    <th>Giá</th>
                                    <th>Hình ảnh</th>
                                    <th>Số lượng</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($product as $key => $pro)
                                    <tr>
                                        <td>
                                            {{$pro->id_product}}

                                        </td>

                                        <td>
                                            {{$pro->category_name}}
                                        </td>

                                        <td>
                                            {{$pro->product_name}}
                                        </td>

                                        <td>
                                            {{$pro->colors}}
                                        </td>

                                        <td>
                                            {{$pro->sale}}
                                        </td>

                                        <td>
                                            {{$pro->title}}
                                        </td>

                                        <td>
                                            {{$pro->highlight}}
                                        </td>

                                        <td>
                                            {{$pro->new_product}}
                                        </td>

                                        <td>
                                            {{$pro->detail}}
                                        </td>

                                        <td>
                                            {{number_format($pro->price)}}
                                        </td>

                                        <td>
                                            <img src="{{asset('public/backend/images/'.$pro->image)}}" width="95" />
                                            <img src="{{asset('public/backend/images/'.$pro->image1)}}" width="95" />
                                            <img src="{{asset('public/backend/images/'.$pro->image2)}}" width="95" />
                                            <img src="{{asset('public/backend/images/'.$pro->image3)}}" width="95" />
                                        </td>

                                        <td>
                                            {{$pro->count}}
                                        </td>

                                        <td>
                                            <a class='btn btn-default btn-sm' style='margin-left:5px'
                                               href="{{URL::to('/admin/edit-product/'.$pro->id_product)}}"><i
                                                    class='fa fa-edit'></i> Sửa</a>
                                            <a onclick="return confirm('Bạn có muốn xóa {{$pro->product_name}}?')"
                                               class='btn btn-danger btn-sm' style='margin-left:5px'
                                               href="{{URL::to('/admin/delete-product/'.$pro->id_product)}}"><i
                                                    class='fa fa-trash'></i> Xóa</a>
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>

@endsection
