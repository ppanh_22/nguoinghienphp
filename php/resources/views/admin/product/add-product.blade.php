@extends('layoutAdmin')
@section('title', 'Thêm sản phẩm')
@section('AdminContent')

    <h2>Thêm sản phẩm</h2>
    <a>
        <?php
        $message = Session::get('message');
        if ($message) {
            echo $message;
            Session::put('message', null);
        }
        ?>
    </a>
    <div class="form-horizontal">
        <hr />
        <form action="{{URL::to('/admin/save-product')}}" method="post">
            {{csrf_field()}}
            <div class="form-group">
                <div class="col-md-10">
                    <label for="username">Tên sản phẩm</label>
                    <input type="text" class="form-control" id="username" name="product_name" REQUIRED>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-10">
                    <label for="username">Tên danh mục</label>
                    <select type="text" class="form-control" id="username" name="product_id_category" REQUIRED>
                        @foreach($category as $key => $cate)
                            <option value="{{$cate->id_category}}">
                                {{$cate->category_name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-10">
                    <label for="username">Màu sắc</label>
                    <input type="text" class="form-control" id="username" name="product_colors" >
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-10">
                    <label for="username">Sale</label>
                    <input type="text" class="form-control" id="username" name="product_sale" >
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-10">
                    <label for="username">Tiêu đề</label>
                    <input type="text" class="form-control" id="username" name="product_title" >
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-10">
                    <label for="username">Highlight</label>
                    <select type="file" class="form-control" id="username" name="product_highlight" >
                            <option value="0">Không</option>
                            <option value="1">Có</option>
                    </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10">
                        <label for="username">Sản phẩm mới</label>
                        <select type="file" class="form-control" id="username" name="product_new_product" >
                                <option value="0">Không</option>
                                <option value="1">Có</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10">
                        <label for="username">Chi tiết</label>
                        <input type="text" class="form-control" id="username" name="product_detail" >
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10">
                        <label for="username">Giá</label>
                        <input type="number" class="form-control" id="username" name="product_price" >
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10">
                        <label for="username">Hình ảnh</label>
                        <input type="file" class="form-control" id="username" name="product_image" >
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10">
                        <label for="username">Hình ảnh 2</label>
                        <input type="file" class="form-control" id="username" name="product_image1" >
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10">
                        <label for="username">Hình ảnh 3</label>
                        <input type="file" class="form-control" id="username" name="product_image2" >
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10">
                        <label for="username">Hình ảnh 4</label>
                        <input type="file" class="form-control" id="username" name="product_image3" >
                    </div>
                </div>
            <div class="form-group">
                <div class="col-md-10">
                    <label for="username">Tổng sản phẩm</label>
                    <input type="text" class="form-control" id="username" name="product_count" >
                </div>
            </div>
                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <a class="btn btn-success" href="{{URL::to('/admin/all-product')}}">Quay lại sản phẩm</a>
                        <input type="submit" name="add-product" value="Tạo mới" class="btn btn-default" />
                    </div>
                </div>
            </form>

        </div>



    @endsection
