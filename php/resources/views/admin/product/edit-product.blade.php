@extends('layoutAdmin')
@section('title', 'Chỉnh sửa sản phẩm')
@section('AdminContent')

    <h2>Sửa sản phẩm</h2>
    <a>
        <?php
        $message = Session::get('message');
        if ($message) {
            echo $message;
            Session::put('message', null);
        }
        ?>
    </a>
    <div class="form-horizontal">
        <hr />
        @foreach($edit_product as $key => $edit_product)
            <form action="{{URL::to('/admin/update-product/'.$edit_product->id_product)}}" method="post">
                {{csrf_field()}}
                <div class="form-group">
                    <div class="col-md-10">
                        <label for="username">Tên sản phẩm</label>
                        <input type="text" class="form-control" id="username" name="product_name" value="{{$edit_product->product_name}}" REQUIRED>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10">
                        <label for="username">Tên danh mục</label>
                        <select type="text" class="form-control" id="username" name="product_id_category" REQUIRED>
                            @foreach($category as $key => $category)
                                <option value="{{$category->id_category}}">
                                    {{$category->category_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10">
                        <label for="username">Màu sắc</label>
                        <input type="text" class="form-control" id="username" name="product_colors" value="{{$edit_product->colors}}" >
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10">
                        <label for="username">Sale</label>
                        <input type="text" class="form-control" id="username" name="product_sale" value="{{$edit_product->sale}}" >
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10">
                        <label for="username">Tiêu đề</label>
                        <input type="text" class="form-control" id="username" name="product_title" value="{{$edit_product->title}}" >
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10">
                        <label for="username">Highlight</label>
                        <select type="file" class="form-control" id="username" name="product_highlight" value="{{$edit_product->highlight}}" >
                            <option value="0">Không</option>--}}
                            <option value="1">Có</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10">
                        <label for="username">Sản phẩm mới</label>
                        <select type="file" class="form-control" id="username" name="product_new_product" value="{{$edit_product->new_product}}" >
                            <option value="0">Không</option>--}}
                            <option value="1">Có</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10">
                        <label for="username">Chi tiết</label>
                        <input type="text" class="form-control" id="username" name="product_detail" value="{{$edit_product->detail}}" >
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10">
                        <label for="username">Giá</label>
                        <input type="number" class="form-control" id="username" name="product_price" value="{{$edit_product->price}}" >
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10">
                        <label for="username">Hình ảnh</label>
{{--                        <input type="text" class="form-control" id="username" name="product_image" value="{{$edit_product->product_image}}" >--}}
                        <input type="file" class="form-control" id="username" name="product_image" value="{{$edit_product->image}}" >
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10">
                        <label for="username">Hình ảnh 2</label>
                        <input type="file" class="form-control" id="username" name="product_image1" value="{{$edit_product->image1}}" >
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10">
                        <label for="username">Hình ảnh 3</label>
                        <input type="file" class="form-control" id="username" name="product_image2" value="{{$edit_product->image2}}" >
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10">
                        <label for="username">Hình ảnh 4</label>
                        <input type="file" class="form-control" id="username" name="product_image3" value="{{$edit_product->image3}}" >
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10">
                        <label for="username">Tổng sản phẩm</label>
                        <input type="text" class="form-control" id="username" name="product_count" value="{{$edit_product->count}}" >
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <a class="btn btn-success" href="{{URL::to('/admin/all-product')}}">Quay lại sản phẩm</a>
                        <input type="submit" name="update-product" value="Cập nhật" class="btn btn-default" />
                    </div>
                </div>
            </form>
        @endforeach
    </div>

    <div>

    </div>



@endsection
