@extends('layoutAdmin')
@section('title', 'Quản lý menu')
@section('AdminContent')
    <h2>Quản lý menu</h2>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <!-- /.card-header -->

                        <div class="card-body">
                            <a class="btn btn-success" onclick="confirmAction()" style="margin-bottom:10px"
                               href="{{URL::to('/admin/add-menu')}}"><i class="fa fa-plus"></i></a>
                            <table id="example1" class="table table-bordered table-striped" method="Get">
                                <thead>
                                <tr>
                                    <th>Mã menu</th>
                                    <th>Tên menu</th>
                                    <th>Đường dẫn</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($menu as $key => $m)
                                    <tr>
                                        <td>
                                            {{$m->id_menu}}

                                        </td>

                                        <td>
                                            {{$m->menu_name}}
                                        </td>

                                        <td>
                                            {{$m->url}}
                                        </td>

                                        <td>
                                            <a class='btn btn-default btn-sm' style='margin-left:5px'
                                               href="{{URL::to('/admin/edit-menu/'.$m->id_menu)}}"><i
                                                    class='fa fa-edit'></i> Sửa</a>
                                            <a onclick="return confirm('Bạn có muốn xóa {{$m->menu_name}}?')"
                                               class='btn btn-danger btn-sm' style='margin-left:5px'
                                               href="{{URL::to('/admin/delete-menu/'.$m->id_menu)}}"><i
                                                    class='fa fa-trash'></i> Xóa</a>
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>

@endsection
