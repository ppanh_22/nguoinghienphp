@extends('layoutAdmin')
@section('title', 'Thêm menu')
@section('AdminContent')

    <h2>Thêm menu</h2>
    <a>
        <?php
        $message = Session::get('message');
        if ($message) {
            echo $message;
            Session::put('message', null);
        }
        ?>
    </a>
    <div class="form-horizontal">
        <hr />
        <form action="{{URL::to('/admin/save-menu')}}" method="post">
            {{csrf_field()}}
            <div class="form-group">
                <div class="col-md-10">
                    <label for="username">Tên menu</label>
                    <input type="text" class="form-control" id="username" name="menu_name" REQUIRED>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-10">
                    <label for="username">Đường dẫn</label>
                    <input type="text" class="form-control" id="username" name="menu_url" REQUIRED>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                    <a class="btn btn-success" href="{{URL::to('/admin/all-menu')}}">Quay lại menu</a>
                    <input type="submit" name="add-menu" value="Tạo mới" class="btn btn-default" />
                </div>
            </div>
        </form>

    </div>

    <div>

    </div>



@endsection
