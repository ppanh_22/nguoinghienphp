@extends('layoutAdmin')
@section('title', 'Chỉnh sửa menu')
@section('AdminContent')

    <h2>Sửa menu</h2>
    <a>
        <?php
        $message = Session::get('message');
        if ($message) {
            echo $message;
            Session::put('message', null);
        }
        ?>
    </a>
    <div class="form-horizontal">
        <hr />
        @foreach($edit_menu as $key => $edit_menu)
            <form action="{{URL::to('/admin/update-menu/'.$edit_menu->id_menu)}}" method="post">
                {{csrf_field()}}
                <div class="form-group">
                    <div class="col-md-10">
                        <label for="username">Tên menu</label>
                        <input type="text" class="form-control" id="username" name="menu_name" value="{{$edit_menu->menu_name}}" REQUIRED>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10">
                        <label for="username">Đường dẫn</label>
                        <input type="text" class="form-control" id="username" name="menu_url" value="{{$edit_menu->url}}" REQUIRED>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <a class="btn btn-success" href="{{URL::to('/admin/all-menu')}}">Quay lại menu</a>
                        <input type="submit" name="update-category" value="Cập nhật" class="btn btn-default" />
                    </div>
                </div>
            </form>
        @endforeach
    </div>

    <div>

    </div>



@endsection
