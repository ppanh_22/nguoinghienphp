@extends('layoutAdmin')
@section('title', 'Thêm size')
@section('AdminContent')

    <h2>Thêm size</h2>
    <a>
        <?php
        $message = Session::get('message');
        if ($message) {
            echo $message;
            Session::put('message', null);
        }
        ?>
    </a>
    <div class="form-horizontal">
        <hr />
        <form action="{{URL::to('/admin/save-size')}}" method="post">
            {{csrf_field()}}
            <div class="form-group">
                <div class="col-md-10">
                    <label for="username">Tên sản phẩm</label>
                    <select type="text" class="form-control" id="username" name="size_product_name" REQUIRED>
                        @foreach($pro as $key => $pro)
                            <option value="{{$pro->id_product}}">
                                {{$pro->product_name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-10">
                    <label for="username">Size</label>
                    <input type="text" class="form-control" id="username" name="size_name" REQUIRED>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-10">
                    <label for="username">Số lượng</label>
                    <input type="file" class="form-control" id="username" name="size_quantity" REQUIRED>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                    <a class="btn btn-success" href="{{URL::to('/admin/all-size')}}">Quay lại size</a>
                    <input type="submit" name="add-size" value="Tạo mới" class="btn btn-default" />
                </div>
            </div>
        </form>

    </div>

    <div>

    </div>



@endsection
