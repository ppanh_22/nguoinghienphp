@extends('layoutAdmin')
@section('title', 'Chỉnh sửa size')
@section('AdminContent')

    <h2>Sửa size</h2>
    <a>
        <?php
        $message = Session::get('message');
        if ($message) {
            echo $message;
            Session::put('message', null);
        }
        ?>
    </a>
    <div class="form-horizontal">
        <hr />
        @foreach($edit_size as $key => $edit_size)
            <form action="{{URL::to('/admin/update-size/'.$edit_size->id_size)}}" method="post">
                {{csrf_field()}}
                <div class="form-group">
                    <div class="col-md-10">
                        <label for="username">Tên sản phẩm</label>
                        <select type="text" class="form-control" id="username" name="size_id_product" REQUIRED>
                        @foreach($pro as $key => $pro)
                            <option value="{{$pro->id_product}}">
                                {{$pro->product_name}}</option>
                        @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10">
                        <label for="username">Tên size</label>
                        <input type="text" class="form-control" id="username" name="size_name" value="{{$edit_size->name_size}}" REQUIRED>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10">
                        <label for="username">Số lượng</label>
                        <input type="text" class="form-control" id="username" name="size_quantity" value="{{$edit_size->quantity}}" REQUIRED>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <a class="btn btn-success" href="{{URL::to('/admin/all-size')}}">Quay lại size</a>
                        <input type="submit" name="update-category" value="Cập nhật" class="btn btn-default" />
                    </div>
                </div>
            </form>
        @endforeach
    </div>

    <div>

    </div>



@endsection
