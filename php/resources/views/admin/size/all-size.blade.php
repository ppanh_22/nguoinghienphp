@extends('layoutAdmin')
@section('title', 'Quản lý size')
@section('AdminContent')
    <h2>Quản lý size</h2>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <!-- /.card-header -->

                        <div class="card-body">
                            <a class="btn btn-success" onclick="confirmAction()" style="margin-bottom:10px"
                               href="{{URL::to('/admin/add-size')}}"><i class="fa fa-plus"></i></a>
                            <table id="example1" class="table table-bordered table-striped" method="Get">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Tên sản phẩm</th>
                                    <th>Size</th>
                                    <th>Số lượng</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($size as $key => $s)
                                    <tr>
                                        <td>
                                            {{$s->id_size}}

                                        </td>

                                        <td>
                                            {{$s->product_name}}
                                        </td>

                                        <td>
                                            {{$s->name_size}}
                                        </td>

                                        <td>
                                            {{$s->quantity}}
                                        </td>

                                        <td>
                                            <a class='btn btn-default btn-sm' style='margin-left:5px'
                                               href="{{URL::to('/admin/edit-size/'.$s->id_size)}}"><i
                                                    class='fa fa-edit'></i> Sửa</a>
                                            <a onclick="return confirm('Bạn có muốn xóa sản phẩm {{$s->product_name}} size {{$s->name_size}}?')"
                                               class='btn btn-danger btn-sm' style='margin-left:5px'
                                               href="{{URL::to('/admin/delete-size/'.$s->id_size)}}"><i
                                                    class='fa fa-trash'></i> Xóa</a>
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>

@endsection
