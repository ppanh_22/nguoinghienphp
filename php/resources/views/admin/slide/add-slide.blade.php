@extends('layoutAdmin')
@section('title', 'Thêm slide')
@section('AdminContent')

    <h2>Thêm slide</h2>
    <a>
        <?php
        $message = Session::get('message');
        if ($message) {
            echo $message;
            Session::put('message', null);
        }
        ?>
    </a>
    <div class="form-horizontal">
        <hr />
        <form action="{{URL::to('/admin/save-slide')}}" method="post">
            {{csrf_field()}}
            <div class="form-group">
                <div class="col-md-10">
                    <label for="username">Hình ảnh</label>
                    <input type="file" class="form-control" id="username" name="slide_img" >
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-10">
                    <label for="username">Tiêu đề</label>
                    <input type="text" class="form-control" id="username" name="slide_caption">
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-10">
                    <label for="username">Nội dung</label>
                    <input type="text" class="form-control" id="username" name="slide_content">
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                    <a class="btn btn-success" href="{{URL::to('/admin/all-slide')}}">Quay lại slide</a>
                    <input type="submit" name="add-slide" value="Tạo mới" class="btn btn-default" />
                </div>
            </div>
        </form>

    </div>

    <div>

    </div>



@endsection
