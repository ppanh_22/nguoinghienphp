@extends('layoutAdmin')
@section('title', 'Quản lý slide')
@section('AdminContent')
    <h2>Quản lý slide</h2>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <!-- /.card-header -->

                        <div class="card-body">
                            <a class="btn btn-success" onclick="confirmAction()" style="margin-bottom:10px"
                               href="{{URL::to('/admin/add-slide')}}"><i class="fa fa-plus"></i></a>
                            <table id="example1" class="table table-bordered table-striped" method="Get">
                                <thead>
                                <tr>
                                    <th>Mã slide</th>
                                    <th>Hình ảnh</th>
                                    <th>Tiêu đề</th>
                                    <th>Nội dung</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($slide as $key => $sli)
                                    <tr>
                                        <td>
                                            {{$sli->id_slide}}

                                        </td>

                                        <td>
                                            <img src="{{asset('public/backend/images/'.$sli->img)}}" width="95" />
                                        </td>

                                        <td>
                                            {{$sli->caption}}
                                        </td>

                                        <td>
                                            {{$sli->content}}
                                        </td>

                                        <td>
                                            <a class='btn btn-default btn-sm' style='margin-left:5px'
                                               href="{{URL::to('/admin/edit-slide/'.$sli->id_slide)}}"><i
                                                    class='fa fa-edit'></i> Sửa</a>
                                            <a onclick="return confirm('Bạn dó muốn xóa slide này?')"
                                               class='btn btn-danger btn-sm' style='margin-left:5px'
                                               href="{{URL::to('/admin/delete-slide/'.$sli->id_slide)}}"><i
                                                    class='fa fa-trash'></i> Xóa</a>
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>

@endsection
