@extends('layoutAdmin')
@section('title', 'Chỉnh sửa slide')
@section('AdminContent')

    <h2>Sửa slide</h2>
    <a>
        <?php
        $message = Session::get('message');
        if ($message) {
            echo $message;
            Session::put('message', null);
        }
        ?>
    </a>
    <div class="form-horizontal">
        <hr />
        @foreach($edit_slide as $key => $edit_slide)
            <form action="{{URL::to('/admin/update-slide/'.$edit_slide->id_slide)}}" method="post">
                {{csrf_field()}}
                <div class="form-group">
                    <div class="col-md-10">
                        <label for="username">Hình ảnh</label>
                        <input type="file" class="form-control" id="username" name="slide_img" value="{{$edit_slide->img}}" REQUIRED>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10">
                        <label for="username">Tiêu đề</label>
                        <input type="text" class="form-control" id="username" name="slide_caption" value="{{$edit_slide->caption}}" REQUIRED>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10">
                        <label for="username">Nội dung</label>
                        <input type="text" class="form-control" id="username" name="slide_content" value="{{$edit_slide->content}}">
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <a class="btn btn-success" href="{{URL::to('/admin/all-slide')}}">Quay lại slide</a>
                        <input type="submit" name="update-category" value="Cập nhật" class="btn btn-default" />
                    </div>
                </div>
            </form>
        @endforeach
    </div>

    <div>

    </div>



@endsection
