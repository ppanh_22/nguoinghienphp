@extends('layoutAdmin')
@section('title', 'Quản lý tài khoản')
@section('AdminContent')
    <h2>Quản lý tài khoản</h2>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <!-- /.card-header -->

                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped" method="Get">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Tên tài khoản</th>
                                    <th>Số điện thoại</th>
                                    <th>Email</th>
                                    <th>Địa chỉ</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($account as $key => $acc)
                                    <tr>
                                        <td>
                                            {{$acc->id_account}}

                                        </td>

                                        <td>
                                            {{$acc->account_name}}
                                        </td>

                                        <td>
                                            {{$acc->phone}}
                                        </td>

                                        <td>
                                            {{$acc->email}}
                                        </td>

                                        <td>
                                            {{$acc->address}}
                                        </td>

{{--                                        <td>--}}

{{--                                            <a class="btn btn-success btn-sm" style="margin-left:5px"--}}
{{--                                               href=""><i--}}
{{--                                                    class="fa fa-details"></i>Chi tiết</a>--}}
{{--                                            <a class='btn btn-default btn-sm' style='margin-left:5px'--}}
{{--                                               href="{{URL::to('/admin/edit-category/'.$cate->id_category)}}"><i--}}
{{--                                                    class='fa fa-edit'></i> Sửa</a>--}}
{{--                                            <a onclick="return confirm('Bạn có muốn xóa {{$cate->category_name}}?')"--}}
{{--                                               class='btn btn-danger btn-sm' style='margin-left:5px'--}}
{{--                                               href="{{URL::to('/admin/delete-category/'.$cate->id_category)}}"><i--}}
{{--                                                    class='fa fa-trash'></i> Xóa</a>--}}
{{--                                        </td>--}}

                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>

@endsection
