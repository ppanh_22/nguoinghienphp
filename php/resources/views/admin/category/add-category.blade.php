@extends('layoutAdmin')
@section('title', 'Thêm danh mục')
@section('AdminContent')

    <h2>Thêm danh mục</h2>
    <a>
        <?php
        $message = Session::get('message');
        if ($message) {
            echo $message;
            Session::put('message', null);
        }
        ?>
    </a>
    <div class="form-horizontal">
        <hr />
        <form action="{{URL::to('/admin/save-category')}}" method="post">
            {{csrf_field()}}
            <div class="form-group">
                <div class="col-md-10">
                    <label for="username">Tên danh mục</label>
                    <input type="text" class="form-control" id="username" name="category_name" REQUIRED>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-10">
                    <label for="username">Mo ta</label>
                    <input type="text" class="form-control" id="username" name="category_mota" REQUIRED>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-10">
                    <label for="username">img</label>
                    <input type="file" class="form-control" id="username" name="category_img" >
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                    <a class="btn btn-success" href="{{URL::to('/admin/all-category')}}">Quay lại danh mục</a>
                    <input type="submit" name="add-category" value="Tạo mới" class="btn btn-default" />
                </div>
            </div>
        </form>

    </div>

    <div>

    </div>



@endsection
