@extends('layoutAdmin')
@section('title', 'Chỉnh sửa danh mục')
@section('AdminContent')

    <h2>Sửa danh mục</h2>
    <a>
        <?php
        $message = Session::get('message');
        if ($message) {
            echo $message;
            Session::put('message', null);
        }
        ?>
    </a>
    <div class="form-horizontal">
        <hr />
        @foreach($edit_category as $key => $edit_category)
        <form action="{{URL::to('/admin/update-category/'.$edit_category->id_category)}}" method="post">
            {{csrf_field()}}
            <div class="form-group">
                <div class="col-md-10">
                    <label for="username">Tên danh mục</label>
                    <input type="text" class="form-control" id="username" name="category_name" value="{{$edit_category->category_name}}" REQUIRED>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-10">
                    <label for="username">Mô tả</label>
                    <input type="text" class="form-control" id="username" name="category_mota" value="{{$edit_category->mota}}" REQUIRED>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-10">
                    <label for="username">img</label>
                    <input type="file" class="form-control" id="username" value="{{$edit_category->img}}" name="category_img" >
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                    <a class="btn btn-success" href="{{URL::to('/admin/all-category')}}">Quay lại danh mục</a>
                    <input type="submit" name="update-category" value="Cập nhật" class="btn btn-default" />
                </div>
            </div>
        </form>
        @endforeach
    </div>

    <div>

    </div>



@endsection
