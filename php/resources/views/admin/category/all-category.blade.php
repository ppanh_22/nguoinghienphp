@extends('layoutAdmin')
@section('title', 'Quản lý danh mục')
@section('AdminContent')
    <h2>Quản lý danh mục</h2>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <!-- /.card-header -->

                        <div class="card-body">
                            <a class="btn btn-success" onclick="confirmAction()" style="margin-bottom:10px"
                               href="{{URL::to('/admin/add-category')}}"><i class="fa fa-plus"></i></a>
                            <table id="example1" class="table table-bordered table-striped" method="Get">
                                <thead>
                                <tr>
                                    <th>Mã danh mục</th>
                                    <th>Tên danh mục</th>
                                    <th>Mô tả</th>
                                    <th>Hình ảnh</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($category as $key => $cate)
                                    <tr>
                                        <td>
                                            {{$cate->id_category}}

                                        </td>

                                        <td>
                                            {{$cate->category_name}}
                                        </td>

                                        <td>
                                            {{$cate->mota}}
                                        </td>

                                        <td>
                                            <img src="{{asset('public/backend/images/'.$cate->img)}}" width="95">

                                        </td>
                                        <td>
                                            <a class='btn btn-default btn-sm' style='margin-left:5px'
                                               href="{{URL::to('/admin/edit-category/'.$cate->id_category)}}"><i
                                                    class='fa fa-edit'></i> Sửa</a>
                                            <a onclick="return confirm('Bạn có muốn xóa {{$cate->category_name}}?')"
                                               class='btn btn-danger btn-sm' style='margin-left:5px'
                                               href="{{URL::to('/admin/delete-category/'.$cate->id_category)}}"><i
                                                    class='fa fa-trash'></i> Xóa</a>
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>

@endsection
