<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Trang quản lý</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('public/backend/AdminLTE-3.1.0/plugins/fontawesome-free/css/all.min.css')}}">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{asset('public/backend/AdminLTE-3.1.0/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('public/backend/AdminLTE-3.1.0/dist/css/adminlte.min.css')}}">
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <!-- /.login-logo -->
    <div class="card card-outline card-primary">
        <div class="card-header text-center">
            <a class="h1">NN SHOES</a>
        </div>
        <div class="card-body">
            <p class="login-box-msg">Đăng nhập</p>
            <?php
            $message = Session::get('message');
            if($message){
                echo $message;
                Session::put('message', null);
            }
            ?>
            <form action="{{URL::to('/admin-dashboard')}}" method="post">
                {{csrf_field()}}
                <div class="input-group mb-3">
                    <input type="text" class="form-control" name="admin_email" placeholder="Email" REQUIRED>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                        </div>
                    </div>
                </div>
                <div class="input-group mb-3">
                    <input type="password" class="form-control" name="admin_password" placeholder="Mật khẩu" REQUIRED>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-8">
                        <div class="icheck-primary">
                            <input type="checkbox" id="remember">
                            <label for="remember">
                                Ghi nhớ đăng nhập
                            </label>
                        </div>

                    </div>
                </div>
                <div class="clearfix"></div>
                <input type="submit" class="btn btn-primary btn-block" value="Đăng nhập" name="login">
{{--                <p class="mb-1">--}}
{{--                    <a href="forgot-password.html">Quên mật khẩu</a>--}}
{{--                </p>--}}

            </form>

        </div>
            <!-- /.social-auth-links -->


        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="{{asset('public/backend/AdminLTE-3.1.0/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('public/backend/AdminLTE-3.1.0/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('public/backend/AdminLTE-3.1.0/dist/js/adminlte.min.js')}}'"></script>
</body>
</html>
