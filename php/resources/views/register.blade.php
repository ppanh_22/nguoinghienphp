<!--
Author: Colorlib
Author URL: https://colorlib.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
<title>Đăng Ký Tài Khoản</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Custom Theme files -->
<link rel="stylesheet" type="text/css" href="{{asset('public/backend/css/register.css')}}">
<!-- //Custom Theme files -->
<!-- web font -->
<link href="//fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,700,700i" rel="stylesheet">
<!-- //web font -->
</head>
<body>
	<!-- main -->
	<div class="main-w3layouts wrapper">
		<h1>Đăng ký tài khoản</h1>
		<div class="main-agileinfo">
			<div class="agileits-top">
                    @if ( $errors->count() > 0 )

                @foreach( $errors->all() as $warning )
                <h4 style="color: gold; font-weight: bolder;font-size: larger;">{{ $warning }}</h4>
                @endforeach
                        @endif

              
				<form action="{{URL::to('/create-account')}}" method="POST">
                    {{csrf_field()}}
					<input class="text" type="text" name="account_name" placeholder="Tên dăng nhập" pattern="^(?=.{8,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$" required="">
                    <br>
                    <input class="text" type="password" name="password" placeholder="Mật khẩu"  pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Phải chứa ít nhất một số và một chữ hoa và chữ thường và ít nhất 8 ký tự trở lên" required>
                    <br>
					<input class="text" type="email" name="email" placeholder="Email" pattern="^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$" title="Email không đúng định dạng" required="">
                    <br>
                    <input class="text" type="text" name="phone" placeholder="Điện thoại" pattern="^(0?)(3[2-9]|5[6|8|9]|7[0|6-9]|8[0-6|8|9]|9[0-4|6-9])[0-9]{7}$" required required="">				
                    <br>
                    <input class="text" type="text" name="address" placeholder="Địa chỉ" required="">
					<div class="wthree-text">					
						<div class="clear"> </div>
					</div>
					<input type="submit" value="Đăng Ký">
				</form>
				<p>Đã có tài khoản ? <a href="{{URL::to('/login')}}"> Đăng Nhập Ngay!</a></p>
			</div>
		</div>

		<ul class="colorlib-bubbles">
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
		</ul>
	</div>
	<!-- //main -->
</body>
</html>