@extends('layout')
@section('title', "Trang chủ")
@section('content')

    <!-- Start Banner Hero -->
    <div id="template-mo-zay-hero-carousel" class="carousel slide" data-bs-ride="carousel">
        <ol class="carousel-indicators">
            <li data-bs-target="#template-mo-zay-hero-carousel" data-bs-slide-to="0" class="active"></li>
            <li data-bs-target="#template-mo-zay-hero-carousel" data-bs-slide-to="1"></li>
            <li data-bs-target="#template-mo-zay-hero-carousel" data-bs-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <div class="container">
                    <div class="row p-5">                        
                            <img class="img-fluid" src="{{URL::to('public/backend/images/phuong.jpg')}}" alt="" style="width: 2200px; height: 591px">                
                    </div>
                </div>
            </div>
            
            @foreach($slides as $key => $slides)
            <div class="carousel-item">
                <div class="container">
                    <div class="row p-5">
                            <img class="img-fluid" src="{{URL::to('public/backend/images/'.$slides->img)}}" alt="" style="width: 2200px; height: 591px">
                    </div>
                </div>
            </div>
            @endforeach
           
        </div>
        <a class="carousel-control-prev text-decoration-none w-auto ps-3" href="#template-mo-zay-hero-carousel" role="button" data-bs-slide="prev">
            <i class="fas fa-chevron-left"></i>
        </a>
        <a class="carousel-control-next text-decoration-none w-auto pe-3" href="#template-mo-zay-hero-carousel" role="button" data-bs-slide="next">
            <i class="fas fa-chevron-right"></i>
        </a>
    </div>
    <!-- End Banner Hero -->

<!-- Start Categories of The Month -->
<section class="container py-5">
    <div class="row text-center pt-3">
        <div class="col-lg-6 m-auto">
            <h1 class="h1">Sản phẩm nổi bật</h1>
            <p>
                Các sản phẩm được giới trẻ ưa thích và được săn đón nhiều nhất
            </p>
        </div>
    </div>
    <div class="row">
        @foreach($pro as $key => $product)
            <div class="col-12 col-md-4 mb-4">
                <div class="card h-100">
                    <a href="{{URL::to('detailproduct/'.$product->id_product)}}">
                        <img src="{{URL::to('public/backend/images/'.$product->image)}}" class="card-img-top" alt="..." style="width: 414px; height: 414px">
                    </a>
                    <div class="card-body">
                        <ul class="list-unstyled d-flex justify-content-between">
                            <li>
                                <i class="text-warning fa fa-star"></i>
                                <i class="text-warning fa fa-star"></i>
                                <i class="text-warning fa fa-star"></i>
                                <i class="text-muted fa fa-star"></i>
                                <i class="text-muted fa fa-star"></i>
                            </li>
                            <li class="text-muted text-right">{{number_format($product->price).'VND'}}</li>
                        </ul>
                        <a href="{{URL::to('detailproduct/'.$product->id_product)}}" class="h2 text-decoration-none text-dark">{{$product->product_name}}</a>
                        <p class="card-text">
                            {{$product->detail}}
                        </p>
                        <p class="text-muted">Lượt xem: 99</p>
                    </div>

                </div>

            </div>
        @endforeach

    </div>
</section>
<!-- End Categories of The Month -->


<!-- Start Featured Product -->
<section class="bg-light">
    <div class="container py-5">
        <div class="row text-center py-3">
            <div class="col-lg-6 m-auto">
                <h1 class="h1">Sản phẩm mới</h1>
                <p>
                    Các sản phẩm của shop luôn luôn cập nhật trên website thường xuyên 24/24
                </p>
            </div>
        </div>
        <div class="row">
            @foreach($newpro as $key => $prod)
            <div class="col-12 col-md-4 mb-4">

                <div class="card h-100">
                    <a href="{{URL::to('detailproduct/'.$prod->id_product)}}">
                        <img src="{{URL::to('public/backend/images/'.$prod->image)}}" class="card-img-top" alt="...">
                    </a>
                    <div class="card-body">
                        <ul class="list-unstyled d-flex justify-content-between">
                            <li>
                                <i class="text-warning fa fa-star"></i>
                                <i class="text-warning fa fa-star"></i>
                                <i class="text-warning fa fa-star"></i>
                                <i class="text-muted fa fa-star"></i>
                                <i class="text-muted fa fa-star"></i>
                            </li>
                            <li class="text-muted text-right">{{number_format($prod->price).'VND'}}</li>
                        </ul>
                        <a href="{{URL::to('detailproduct/'.$prod->id_product)}}" class="h2 text-decoration-none text-dark">{{$prod->product_name}}</a>
                        <p class="card-text">
                            {{$prod->detail}}
                        </p>
                        <p class="text-muted">Lượt xem: 99</p>
                    </div>

                </div>

            </div>
            @endforeach

        </div>
    </div>
</section>
<!-- End Featured Product -->
@endsection
