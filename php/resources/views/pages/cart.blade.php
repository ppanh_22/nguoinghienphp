@extends('layout')
@section('title', 'Giỏ hàng')
@section('content')
    <!-- Open Content -->
    <section class="bg-light" style="padding-bottom: 20px">
        @php
            $cart = Session::get('cart');
            $id_account = Session::get('id_account');
            $email = Session::get('account_email');
            $name = Session::get('account_name');
            $phone = Session::get('account_phone');
            $address = Session::get('account_address');

            $total = 0;
            if (isset($cart)) {
                foreach ($cart as $key => $value) {
                    $total += $value['price'] * $value['quantity'];
                }
            }
        @endphp
        @if (isset($cart))
            @foreach ($cart as $item)
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <img src="{{ $item['image'] }}" alt="" class="img-fluid">
                                        </div>
                                        <div class="col-md-4">
                                            <h5 class="card-title"><b>Tên giày:  </b>{{ $item['name'] }}</h5>
                                            <p class="card-text"><b>Giá giày:  </b>{{ $item['price'] }}</p>
                                            <p class="card-text"><b>Size giày:  </b>{{ $item['size'] }}</p>
                                            <p class="card-text"><b>Số lượng:  </b>{{ $item['quantity'] }}</p>
                                        </div>
                                        <div class="col-md-6">
                                            <a href="{{ URL::to('/delete-cart/' . $item['cartItemId']) }}" type="button">
                                                <i class="fa fa-trash" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            <input type="hidden" id="checkAuth" value={{ $name }}>
            <form action={{URL::to('/place-order')}} method="post" onsubmit="return checkAuth()">
                {{ csrf_field() }}
                <div class="container" style="margin-top: 20px">
                    <h5 class="card-title">Tổng tiền: {{ $total }}</h5>
                    <div class="row">
                        <input placeholder="" type="hidden" name="total" value={{ $total }}>
                    </div>
                    <div class="row">
                        <input placeholder="Tên người nhận" type="text" name="shipname" value="{{ $name }}" required />
                    </div>
                    <div class="row">
                        <input placeholder="Địa chỉ nhận hàng" type="text" name="shipaddress" value="{{ $address }}" required>
                    </div>
                    <div class="row">
                        <input placeholder="SĐT nhận hàng" type="text" name="shipphone" value="{{ $phone }}" required>
                    </div>
                    <div class="row">
                        <input placeholder="Email nhận thông báo" type="text" name="shipemail" value="{{ $email }}" required>
                    </div>
                    <div class="row">
                        <textarea placeholder="Ghi chú" name="shipnote" id="" cols="30" rows="10"></textarea>
                    </div>
                    <div class="row">
                        <button type="submit" class="btn btn-primary">Thanh toán</button>
                    </div>
                </div>
            </form>
        @else
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 class="card-title">Giỏ hàng rỗng</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        @endif


    </section>
    <!-- Close Content -->
@endsection
