@extendS('layout')
@section('title', 'Trang giới thiệu')
@section('content')



    <!-- Modal -->
    <div class="modal fade bg-white" id="templatemo_search" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="w-100 pt-1 mb-5 text-right">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="" method="get" class="modal-content modal-body border-0 p-0">
                <div class="input-group mb-2">
                    <input type="text" class="form-control" id="inputModalSearch" name="q" placeholder="Search ...">
                    <button type="submit" class="input-group-text bg-success text-light">
                        <i class="fa fa-fw fa-search text-white"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>


    <!-- Start Content Page -->
    <div class="container-fluid bg-light py-5">
        <div class="col-md-6 m-auto text-center">
            <h1 class="h1">Liên hệ ngay với chúng tôi</h1>
            <p>
                NN Shoes - Mang tới cho bạn những đôi giày chất lượng và giá tốt nhất.
                Hãy cho chúng tôi biết cảm nhận của bạn để chúng mình cố gắng hoàn thiện và mang tới cho bạn dịch vụ tốt nhất nhé.
            </p>
        </div>
    </div>



    <!-- Start Contact -->
    <div class="container py-5">
        <div class="row py-5">
        <form class="col-md-9 m-auto" method="post" role="form" action="{{URL::to('/contact-save')}}">
            {{csrf_field()}}
                <div class="row">
                    <div class="form-group col-md-6 mb-3">
                        <label for="inputname">Họ tên</label>
                        <input type="text" class="form-control mt-1" id="name" name="name" placeholder="Nhập họ tên...">
                    </div>
                    <div class="form-group col-md-6 mb-3">
                        <label for="inputemail">Email</label>
                        <input type="email" class="form-control mt-1" id="email" name="email" placeholder="Nhập email...">
                    </div>
                </div>
                <div class="mb-3">
                    <label for="inputsubject">Tiêu đề</label>
                    <input type="text" class="form-control mt-1" id="subject" name="title" placeholder="Nhập tiêu đề...">
                </div>
                <div class="mb-3">
                    <label for="inputmessage">Nội dung</label>
                    <textarea class="form-control mt-1" id="message" name="description" placeholder="Nhập nội dung..." rows="8"></textarea>
                </div>
                <div class="row">
                    <div class="col text-end mt-2">
                        <button type="submit" class="btn btn-success btn-lg px-3">Liên hệ ngay</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- End Contact -->


@endsection
