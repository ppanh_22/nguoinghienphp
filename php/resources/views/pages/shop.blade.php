@extends('layout')
@section('title', 'Trang sản phẩm')
@section('content')

<!-- Modal -->
<div class="modal fade bg-white" id="templatemo_search" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="w-100 pt-1 mb-5 text-right">
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <form action="{{URL::to('/search-product')}}" method="POST" class="modal-content modal-body border-0 p-0">
            <div class="input-group mb-2">
                <input type="text" class="form-control" id="inputModalSearch" name="name_product" placeholder="Search ...">
                <button type="submit" class="input-group-text bg-success text-light">
                    <i class="fa fa-fw fa-search text-white"></i>
                </button>
            </div>
        </form>
    </div>
</div>



<!-- Start Content -->
<div class="container py-5">
    <div class="row">
        <div class="col-lg-3">
				<h1>Thương Hiệu</h1>
				<ul id="accordion" class="accordion">
                    @foreach($category as $key => $cate)
					<li>
						<div class="link">
							<i class="fa fa-shoe-prints"></i>
							<a href="{{URL::to('product-cate/'.$cate->id_category)}}">{{$cate->category_name}}</a>
						</div>
					</li>
                    @endforeach
				</ul>
			</div>

        <div class="col-lg-9">
            <div class="row">
                <div class="col-md-6 pb-4">
                    <div class="d-flex">

                    </div>
                </div>
            </div>
            <div class="row">
                @foreach($product as $key => $prod)
                <div class="col-md-4">
                    <div class="card mb-4 product-wap rounded-0">
                        <div class="card rounded-0">
                            <img class="card-img rounded-0 img-fluid" src="{{URL::to('public/backend/images/'.$prod->image)}}" style="width: 302px; height: 302px">
                            <div class="card-img-overlay rounded-0 product-overlay d-flex align-items-center justify-content-center">
                                <ul class="list-unstyled">
                                    <li><a class="btn btn-success text-white mt-2" href="{{URL::to('detailproduct/'.$prod->id_product)}}"><i class="far fa-eye"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-body">
                            <a href="{{URL::to('detailproduct/'.$prod->id_product)}}" class="h3 text-decoration-none">{{$prod->product_name}}</a>
                            <ul class="w-100 list-unstyled d-flex justify-content-between mb-0">
                                <li class="pt-2">
                                    <span class="product-color-dot color-dot-red float-left rounded-circle ml-1"></span>
                                    <span class="product-color-dot color-dot-blue float-left rounded-circle ml-1"></span>
                                    <span class="product-color-dot color-dot-black float-left rounded-circle ml-1"></span>
                                    <span class="product-color-dot color-dot-light float-left rounded-circle ml-1"></span>
                                    <span class="product-color-dot color-dot-green float-left rounded-circle ml-1"></span>
                                </li>
                            </ul>
                            <ul class="list-unstyled d-flex justify-content-center mb-1">
                                <li>
                                    <i class="text-warning fa fa-star"></i>
                                    <i class="text-warning fa fa-star"></i>
                                    <i class="text-warning fa fa-star"></i>
                                    <i class="text-muted fa fa-star"></i>
                                    <i class="text-muted fa fa-star"></i>
                                </li>
                            </ul>
                            <p class="text-center mb-0">{{number_format($prod->price)}}</p>
                        </div>
                    </div>
                </div>
                @endforeach

            </div>
            <div div="row">
                {{$product->links()}}
            </div>
        </div>
    </div>
</div>
<!-- End Content -->

<!-- Start Brands -->
<section class="bg-light py-5">
    <div class="container my-4">
        <div class="row text-center py-3">
            <div class="col-lg-6 m-auto">
            <h1 style="font-weight: bold">THƯƠNG HIỆU NỔI TIẾNG</h1>
            <p>
                Những đôi giày đến từ các thương hiệu nổi tiếng trên toàn thế giới. </p>
            <p>
                Shop cam kết mang đến cho bạn những đôi giày chất lượng và những trải nghiệm tuyệt vời nhất.

            </p>
            </div>
            <div class="col-lg-9 m-auto tempaltemo-carousel">
                <div class="row d-flex flex-row">
                    <!--Controls-->
                    <div class="col-1 align-self-center">
                        <a class="h1" href="#multi-item-example" role="button" data-bs-slide="prev">
                            <i class="text-light fas fa-chevron-left"></i>
                        </a>
                    </div>
                    <!--End Controls-->

                    <!--Carousel Wrapper-->
                    <div class="col">
                        <div class="carousel slide carousel-multi-item pt-2 pt-md-0" id="templatemo-slide-brand" data-bs-ride="carousel">
                            <!--Slides-->
                            <div class="carousel-inner product-links-wap" role="listbox">

                                <!--First slide-->
                                <div class="carousel-item active">

                                    <div class="row">
                                        @foreach($category as $key => $cate)
                                            <div class="col-3 p-md-5">
                                                <a href="#"><img class="img-fluid brand-img" src="{{URL::to('public/backend/images/'.$cate->img)}}" alt="Brand Logo"></a>
                                            </div>
                                        @endforeach
                                    </div>

                                </div>
                                <!--End First slide-->

                                <!--Second slide-->
                                <div class="carousel-item">
                                    <div class="row">
                                        @foreach($category as $key => $cate)
                                            <div class="col-3 p-md-5">
                                                <a href="#"><img class="img-fluid brand-img" src="{{URL::to('public/backend/images/'.$cate->img)}}" alt="Brand Logo"></a>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <!--End Second slide-->

                                <!--Third slide-->
                                <div class="carousel-item">
                                    <div class="row">
                                        @foreach($category as $key => $cate)
                                            <div class="col-3 p-md-5">
                                                <a href="#"><img class="img-fluid brand-img" src="{{URL::to('public/backend/images/'.$cate->img)}}" alt="Brand Logo"></a>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <!--End Third slide-->

                            </div>
                            <!--End Slides-->
                        </div>
                    </div>
                    <!--End Carousel Wrapper-->

                    <!--Controls-->
{{--                    <div class="col-1 align-self-center">--}}
{{--                        <a class="h1" href="#multi-item-example" role="button" data-bs-slide="next">--}}
{{--                            <i class="text-light fas fa-chevron-right"></i>--}}
{{--                        </a>--}}
{{--                    </div>--}}
                    <!--End Controls-->
                </div>
            </div>
        </div>
    </div>
</section>
<!--End Brands-->
@endsection
