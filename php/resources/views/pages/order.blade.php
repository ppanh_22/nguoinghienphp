@extends('layout')
@section('title', 'Giỏ hàng')
@section('content')
    <!-- Open Content -->
    <section class="bg-light" style="padding-bottom: 20px">
     <div class="container" style="padding-top: 20px">
         <h1>Đơn hàng của {{$userName}}</h1>
        @foreach ($orders as $order)
        {{-- print order detail here --}}
        <h1>{{$order->created_at}}</h1>
        @foreach ($order->order_detail as $orderDetail)
            <div class="row">
                <div class="col-md-10">
                    <p>{{$orderDetail->product->product_name}} x {{$orderDetail->quantity}}</p>
                </div>
            </div>

        @endforeach
        <div class="row">
            <div class="col-md-10">
                <p>Tổng tiền: {{$order->total_sum}} VND</p>
            </div>
        </div>

     @endforeach
     </div>
    </section>
    <!-- Close Content -->
@endsection
