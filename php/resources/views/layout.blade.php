<!DOCTYPE html>
<html lang="en">

<head>
    <title>NN SHOES</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="{{ asset('public/frontend/img/apple-icon.png') }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/frontend/img/favicon.ico') }}">

    <link rel="stylesheet" href="{{ asset('public/frontend/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/frontend/css/templatemo.css') }}">
    <link rel="stylesheet" href="{{ asset('public/frontend/css/custom.css') }}">

    <!-- Load fonts style after rendering the layout styles -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;200;300;400;500;700;900&display=swap">
    <link rel="stylesheet" href="{{ asset('public/frontend/css/fontawesome.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{asset('public/frontend/css/slick.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/frontend/css/slick-theme.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/backend/css/dropdown.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/backend/css/cate.css')}}">

</head>

<body>
    <!-- Start Top Nav -->
    <nav class="navbar navbar-expand-lg bg-dark navbar-light d-none d-lg-block" id="templatemo_nav_top">
        <div class="container text-light">
            <div class="w-100 d-flex justify-content-between">
                <div>
                    <i class="fa fa-envelope mx-2"></i>
                    <a class="navbar-sm-brand text-light text-decoration-none"
                        href="mailto:nguoinghien@company.com">nguoinghien@company.com</a>
                    <i class="fa fa-phone mx-2"></i>
                    <a class="navbar-sm-brand text-light text-decoration-none" href="tel:0332103162">0332103162</a>
                </div>
            </div>
        </div>
    </nav>
    <!-- Close Top Nav -->


    <!-- Header -->
    <nav class="navbar navbar-expand-lg navbar-light shadow">
        <div class="container d-flex justify-content-between align-items-center">

            <a class="navbar-brand text-success logo h1 align-self-center" href="{{URL::to('/home')}}">
                NN Shoes
            </a>

            <button class="navbar-toggler border-0" type="button" data-bs-toggle="collapse" data-bs-target="#templatemo_main_nav" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="align-self-center collapse navbar-collapse flex-fill  d-lg-flex justify-content-lg-between" id="templatemo_main_nav">
                <div class="flex-fill">
                    <ul class="nav navbar-nav d-flex justify-content-between mx-lg-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="{{URL::to('/home')}}">Trang chủ</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{URL::to('/about')}}">Giới thiệu</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{URL::to('/shop')}}">Shop</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{URL::to('/contact')}}">Liên hệ</a>
                        </li>
                    </ul>
                </div>
                <div class="navbar align-self-center d-flex">
                    <div class="d-lg-none flex-sm-fill mt-3 mb-4 col-7 col-sm-auto pr-3">
                        <div class="input-group">
                            <form action="{{URL::to('/search')}}" method="post">
                                {{csrf_field()}}
                            <input type="text" class="form-control" name="keywords_submit" id="inputMobileSearch" placeholder="Nhập sản phẩm cần tìm...">
                                <input type="submit" value="Tìm kiếm" name="search_item">
                            </form>
                        </div>

                    </div>
                    <a class="nav-icon d-none d-lg-inline" href="#" data-bs-toggle="modal" data-bs-target="#templatemo_search">
                        <i class="fa fa-fw fa-search text-dark mr-2"></i>
                    </a>
                    <a class="nav-icon position-relative text-decoration-none" href="{{URL::to('/cart')}}">
                        <i class="fa fa-fw fa-cart-arrow-down text-dark mr-1"></i>
                        <span class="position-absolute top-0 left-100 translate-middle badge rounded-pill bg-light text-dark"></span>
                    </a>
                    <div class="dropdown">
					  <a class="dropbtn" href="{{URL::to('/login')}}" style="padding-left: 5px; width: 20px"><i class="fa fa-fw fa-user text-dark mr-3"> </i>  </a>

                        <?php
                            $name = Session::get('account_name');
                            if ($name) {
                                $url = route('user.logout_user');
                                $urlOrder = route('order');
                                echo '<i style="width: 20px">  Xin chào: ',$name,'</i>';
                                echo '<div class="dropdown-content">';
                                echo '<a href="',$urlOrder,'">Đơn đã đặt</a>';
                                echo '<a href="', $url,'">Đăng xuất</a> </div>';
                            }else{
                                $url2 = route('user.login');
                                $url3 = route('user.register');
                                echo '<div class="dropdown-content">
                                 <a href="', $url2 ,'">Đăng nhập</a>
                                 <a href="', $url3 ,'">Đăng ký</a>
                                </div>';
                            }
                            ?>
                    </div>
                </div>
            </nav>
            <!-- Close Header -->

        </div>
    </nav>
    <!-- Close Header -->

    <!-- Modal -->
    <div class="modal fade bg-white" id="templatemo_search" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="w-100 pt-1 mb-5 text-right">
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <form action="{{URL::to('/search-product')}}" method="POST" class="modal-content modal-body border-0 p-0">
            {{csrf_field()}}
            <div class="input-group mb-2">
                <input type="text" class="form-control" id="inputModalSearch" name="name_product" placeholder="Search ...">
                <button type="submit" class="input-group-text bg-success text-light">
                    <i class="fa fa-fw fa-search text-white"></i>
                </button>
            </div>
        </form>
    </div>
</div>

    @yield('content')
    <!-- Start Footer -->
    <footer class="bg-dark" id="tempaltemo_footer">
        <div class="container">
            <div class="row">

                        <div class="col-md-4 pt-5">
                            <h2 class="h2 text-success border-bottom pb-3 border-light logo">NN Shoes</h2>
                            <ul class="list-unstyled text-light footer-link-list">
                                <li>
                                    <i class="fas fa-map-marker-alt fa-fw"></i>
                                    Xa Lộ Hà Nội, Quận 9, Thành phố Thủ Đức
                                </li>
                                <li>
                                    <i class="fa fa-phone fa-fw"></i>
                                    <a class="text-decoration-none" href="tel:0332103162">0332103162</a>
                                </li>
                                <li>
                                    <i class="fa fa-envelope fa-fw"></i>
                                    <a class="text-decoration-none"
                                        href="mailto:nguoinghien@company.com">nguoinghien@company.com</a>
                                </li>
                            </ul>
                        </div>

                        <div class="col-md-4 pt-5">
                            <h2 class="h2 text-light border-bottom pb-3 border-light">Sản phẩm</h2>
                            <ul class="list-unstyled text-light footer-link-list">
                                @foreach ($category as $key => $cate)
                                    <li><a class="text-decoration-none" href="{{ URL::to('/product-cate/'.$cate->id_category) }}">
                                            {{ $cate->category_name }}</a></li>
                                @endforeach
                            </ul>
                        </div>

                <div class="col-md-4 pt-5">
                    <h2 class="h2 text-light border-bottom pb-3 border-light">Xem thêm</h2>
                    <ul class="list-unstyled text-light footer-link-list">
                        <li><a class="text-decoration-none" href="{{URL::to('/home')}}">Trang chủ</a></li>
                        <li><a class="text-decoration-none" href="{{URL::to('/shop')}}">Shop</a></li>
                        <li><a class="text-decoration-none" href="{{URL::to('/about')}}">Giới thiệu</a></li>
                        <li><a class="text-decoration-none" href="{{URL::to('/contact')}}">Liên hệ</a></li>
                    </ul>
                </div>

            </div>

            <div class="row text-light mb-4">
                <div class="col-12 mb-3">
                    <div class="w-100 my-3 border-top border-light"></div>
                </div>
                <div class="col-auto me-auto">
                    <ul class="list-inline text-left footer-icons">
                        <li class="list-inline-item border border-light rounded-circle text-center">
                            <a class="text-light text-decoration-none" target="_blank" href="http://facebook.com/"><i class="fab fa-facebook-f fa-lg fa-fw"></i></a>
                        </li>
                        <li class="list-inline-item border border-light rounded-circle text-center">
                            <a class="text-light text-decoration-none" target="_blank" href="https://www.instagram.com/"><i class="fab fa-instagram fa-lg fa-fw"></i></a>
                        </li>
                    </ul>
                </div>

            </div>
        </div>

        <div class="w-100 bg-black py-3">
            <div class="container">
                <div class="row pt-2">
                    <div class="col-12">
                        <p class="text-left text-light">
                            Copyright &copy; 2022 Nguoi Nghien Company
                            | Created by <a rel="sponsored" href="#" target="_blank">NN TEAM</a>
                        </p>
                    </div>
                </div>

            </footer>
            <!-- End Footer -->

            <!-- Start Script -->
            <script src="{{ asset('public/frontend/js/jquery-1.11.0.min.js') }}"></script>
            <script src="{{ asset('public/frontend/js/jquery-migrate-1.2.1.min.js') }}"></script>
            <script src="{{ asset('public/frontend/js/bootstrap.bundle.min.js') }}"></script>
            <script src="{{ asset('public/frontend/js/templatemo.js') }}"></script>
            <script src="{{ asset('public/frontend/js/custom.js') }}"></script>
            <script src="{{ asset('public/frontend/js/slick.min.js') }}"></script>
            <script>
                $('#carousel-related-product').slick({
                    infinite: true,
                    arrows: false,
                    slidesToShow: 4,
                    slidesToScroll: 3,
                    dots: true,
                    responsive: [{
                            breakpoint: 1024,
                            settings: {
                                slidesToShow: 3,
                                slidesToScroll: 3
                            }
                        },
                        {
                            breakpoint: 600,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 3
                            }
                        },
                        {
                            breakpoint: 480,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 3
                            }
                        }
                    ]
                });

                function checkSize() {
                    var size = document.getElementById('product-size').value;

                    if (size == '') {
                        alert('Bạn chưa chọn size');
                        return false;
                    }
                    else{
                        var quantity = document.getElementById('product-quanity').value;
                        var cur_qty = document.getElementById(size).value;
                        if (quantity > cur_qty) {
                            alert('Số lượng sản phẩm không đủ');
                            return false;
                        }
                        else{
                            return true;
                        }
                    }
                }
                function checkAuth() {
                    var auth = document.getElementById('checkAuth').value;
                    if (auth == '') {
                        alert('Bạn chưa đăng nhập');
                        return false;
                    }
                }
            </script>
            <!-- End Script -->
</body>

</html>
