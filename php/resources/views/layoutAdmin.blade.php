<!DOCTYPE html>

<html>
<head>

    <title>Trang quản lý | @yield('title')</title>
    <meta name="viewport" content="width=device-width" />
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" />
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{asset('public/backend/AdminLTE-3.1.0/plugins/fontawesome-free/css/all.min.css')}}">
    <!-- IonIcons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('public/backend/AdminLTE-3.1.0/dist/css/adminlte.min.css')}}">
    <!--Datatables-->
    <link rel="stylesheet" href="{{asset('public/backend/AdminLTE-3.1.0/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/backend/AdminLTE-3.1.0/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/backend/AdminLTE-3.1.0/plugins/datatables-buttons/css/buttons.bootstrap4.min.css')}}">

</head>
<body class="hold-transition sidebar-mini">

<div class="wrapper">
    <!-- Navbar -->

    <!-- /.navbar -->
    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="{{URL::to('/dashboard.blade.php')}}" class="brand-link">
            <img src="{{asset('public/backend/AdminLTE-3.1.0/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
            <span class="brand-text font-weight-light">NN Shoes</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <!-- SidebarSearch Form -->
            <div class="form-inline">
                <div class="input-group" data-widget="sidebar-search">
                    <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
                    <div class="input-group-append">
                        <button class="btn btn-sidebar">
                            <i class="fas fa-search fa-fw"></i>
                        </button>
                    </div>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <!-- Add icons to the links using the .nav-icon class
with font-awesome or any other icon font library -->

                    <li class="nav-item">
                        <a href="{{URL::to('/admin/all-category')}}" class="nav-link">
                            <i class="fas fa-clipboard-list fa-lg"></i>
                            <p>Danh mục</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{URL::to('/admin/all-product')}}" class="nav-link">
                            <i class="fas fa-tags"></i>
                            <p>Sản phẩm</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{URL::to('/admin/all-size')}}" class="nav-link">
                            <i class="fas fa-globe fa-lg"></i>
                            <p>Size</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{URL::to('/admin/all-order')}}" class="nav-link">
                            <i class="fas fa-truck "></i>
                            <p>Đơn hàng</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{URL::to('/admin/all-slide')}}" class="nav-link">
                            <i class="fas fa-warehouse "></i>
                            <p>Slide</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{URL::to('/admin/all-menu')}}" class="nav-link">
                            <i class="fas fa-file-invoice-dollar fa-lg"></i>
                            <p>Menu</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{URL::to('/admin/all-account')}}" class="nav-link">
                            <i class="fas fa-file-invoice-dollar fa-lg"></i>
                            <p>Account</p>
                        </a>
                    </li>

                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">

                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item active">
                                <h4 class="m-0" style="color: #0a0e14">
                                    <?php
                                    $name = Session::get('admin_name');
                                    if($name){
                                        echo $name;
                                    }
                                    ?></h4>
                            </li>
                            <li class="breadcrumb-item"><a href="{{URL::to('/logout')}}">Đăng Xuất</a></li>

                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>

        <div>
            @yield('AdminContent')
        </div>
    </div>
</div>

</body>
<footer class="main-footer">
    <strong>Thực Hiện Bởi Nhóm <a href="https://adminlte.io">Trại Người Nghiện</a></strong>

    <div class="float-right d-none d-sm-inline-block">
        <b>PHP</b>
    </div>
</footer>

<!-- ./wrapper -->
<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<script src="{{asset('public/backend/AdminLTE-3.1.0/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap -->
<script src="{{asset('public/backend/AdminLTE-3.1.0/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE -->
<script src="{{asset('public/backend/AdminLTE-3.1.0/dist/js/adminlte.js')}}"></script>

<!-- OPTIONAL SCRIPTS -->
<script src="{{asset('public/backend/AdminLTE-3.1.0/plugins/chart.js/Chart.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('public/backend/AdminLTE-3.1.0/dist/js/demo.js')}}"></script>
<!-- AdminLTE dashboard.blade.php demo (This is only for demo purposes) -->
<script src="{{asset('public/backend/AdminLTE-3.1.0/dist/js/pages/dashboard3.js')}}"></script>
<!-- jQuery -->
<script src="{{asset('public/backend/AdminLTE-3.1.0/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('public/backend/AdminLTE-3.1.0/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

<script src="{{asset('public/backend/Scripts/jquery-1.12.4.min.js')}}"></script>
<script src="{{asset('public/backend/Scripts/bootstrap.min.js')}}"></script>
<script src="{{asset('public/backend/Scripts/jquery.validate.min.js')}}"></script>
<script src="{{asset('public/backend/Scripts/jquery.validate.unobtrusive.min.js')}}"></script>
<script src="{{asset('public/backend/Scripts/jquery-ui-1.12.1.min.js')}}"></script>
<script src="{{asset('public/backend/Scripts/notify.min.js')}}"></script>

<!-- DataTables  & Plugins -->
<script src="{{asset('public/backend/AdminLTE-3.1.0/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/backend/AdminLTE-3.1.0/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/backend/AdminLTE-3.1.0/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('public/backend/AdminLTE-3.1.0/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('public/backend/AdminLTE-3.1.0/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script src="{{asset('public/backend/AdminLTE-3.1.0/plugins/datatables-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('public/backend/AdminLTE-3.1.0/plugins/datatables-buttons/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{asset('public/backend/AdminLTE-3.1.0/plugins/jszip/jszip.min.js')}}"></script>
<script src="{{asset('public/backend/AdminLTE-3.1.0/plugins/pdfmake/pdfmake.min.js')}}"></script>
<script src="{{asset('public/backend/AdminLTE-3.1.0/plugins/pdfmake/vfs_fonts.js')}}"></script>
<script src="{{asset('public/backend/AdminLTE-3.1.0/plugins/datatables-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('public/backend/AdminLTE-3.1.0/plugins/datatables-buttons/js/buttons.print.min.js')}}"></script>
<script src="{{asset('public/backend/AdminLTE-3.1.0/plugins/datatables-buttons/js/buttons.colVis.min.js')}}"></script>
<script src="{{asset('public/backend/Scripts/ckeditor/ckeditor.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('public/backend/AdminLTE-3.1.0/dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('public/backend/AdminLTE-3.1.0/dist/js/demo.js')}}"></script>

<!-- Page specific script -->
<script>
    $(function () {
        $("#example1").DataTable({
            "responsive": true, "lengthChange": false, "autoWidth": false,
            "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
        });
    });</script>

</html>
